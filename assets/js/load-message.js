var device
if ($(window).width() < 768) {
    device = 'mobile'
} else {
    device = 'desktop'
}
$(window).resize(function() {
    if ($(window).width() < 768) {
        device = 'mobile'
        initialCallForMessage()
    } else {
        device = 'desktop'
        initialCallForMessage()
    }
});
// console.log(device)

var page
function initialCallForMessage () {
    page = 1
    $.ajax({
        url: "assets/js/testData.js",
        method: "GET",
        success: function(result) {
            // console.log(device)
            // console.log('init call')
            data = JSON.parse(result)

            // console.log(data.length)
            if (data.length == 0) {
                $('.loading-more').removeClass('infinite')
                $('.loading-more').html('目前尚無留言')
                // $('.separate').css('display', 'none')
                // $('.message-board').css('display', 'none')
                return
            }
            var html = sortingData(device, data)
            $('#message-content').html(html)
            if (device === 'desktop') {
                var height = document.getElementById('message-content').scrollHeight
                $('.message-content').scrollTop(height - 300)
                $('.message-content').animate({
                    scrollTop: height
                },4000)
            }
            if (data.length < 4) {
                $('.loading-more').removeClass('infinite')
                $('.loading-more').html('沒有更多留言囉')
                // $('.separate').css('display', 'none')
                // $('.message-board').css('display', 'none')
                return
            }
        },
        error: function(error) {
            console.error(error);
            $('.separate').fadeOut()
        }
    });
};

function sortingData (device, data) {
    data = data.sort(function(a, b) {
        if (device === 'desktop') {
            return (a['id'] > b['id']) ? 1 : ((a['id'] < b['id']) ? -1 : 0);
        } else {
            return (b['id'] > a['id']) ? 1 : ((b['id'] < a['id']) ? -1 : 0);
        }
    })
    return convertData(data)
}

function convertData (data) {
    var htmlString = '' 
    for (var i = 0; i < data.length; i++) {
        // console.log(data[i].id)
        htmlString += '<div class="message" style="margin-bottom: 10px;">'
        htmlString += '<p class="floor sans"><span class="arial">' + data[i].id + '</span> 樓</p>'
        htmlString += '<p class="text sans">' + data[i].content + '</p>'
        htmlString += '<p class="date arial">' + data[i].updated_at + '</p>'
        htmlString += '</div>'
        if (data[i].reply) {
            htmlString += '<div class="reply">'
            htmlString += '<div class="reply-girl-pic" alt=""></div>'
            htmlString += '<p class="text">' + data[i].reply.content + '</p>'
            htmlString += '</div><hr>'
        }
        // else {
        //     htmlString += '<hr>'
        // }
    }
    return htmlString
}

var ready = 1

$('.message-content').scroll(function(){
    var message = document.getElementById('message-content')
    scrollHeight = message.scrollHeight
    console.log($('.message-content').scrollTop())
    console.log(scrollHeight)
    if (device === 'desktop' && ready === 1) {
        if ($('.message-content').scrollTop() < 10) {
            console.log('load more')
            loadMoreData()    
        }
    } else if (ready === 1) {
        if ($('.message-content').scrollTop() + 800 > scrollHeight) {
            console.log('load more')
            loadMoreData()
        }
    }
})

function loadMoreData () {
    ready = 0
    page += 1
    console.log(page)
    var existedHTML = $('#message-content').html()
    var result
    $.ajax({
        url: "assets/js/testData.js#page=" + page,
        method: "GET",
        success: function(result) {
            console.log('loading more')
            data = JSON.parse(result)
            if (data.length == 0) {
                $('.loading-more').removeClass('infinite')
                $('.loading-more').html('沒有更多留言囉')
                // $('.separate').css('display', 'none')
                // $('.message-board').css('display', 'none')
                return
            }
            var newHTML = sortingData(device, data)
            if (device === 'desktop') {
                result = newHTML + existedHTML
                setTimeout(function(){
                    $('#message-content').html(result)
                    $('.message-content').scrollTop(400)
                    $('.message-content').animate({
                        scrollTop: 100
                    },200)
                    if (data.length < 4) {
                        $('.loading-more').removeClass('infinite')
                        $('.loading-more').html('沒有更多留言囉')
                        // $('.separate').css('display', 'none')
                        // $('.message-board').css('display', 'none')
                    }
                    ready = 1
                },1000)
            } else {
                result =  existedHTML + newHTML
                setTimeout(function(){
                    $('#message-content').html(result)
                    if (data.length < 4) {
                        $('.loading-more').removeClass('infinite')
                        $('.loading-more').html('沒有更多留言囉')
                        // $('.separate').css('display', 'none')
                        // $('.message-board').css('display', 'none')
                    }
                    ready = 1
                },1000)
            }
        },
        error: function(error) {
            console.error(error);
        }
    });
}

initialCallForMessage()